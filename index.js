var consolidate = require('consolidate');
var stream = require('stream');

for (var name in consolidate) {
    
    module.exports[name] = (function (name) {
        
        return function (path) {
            
            return new stream.PassThrough({
                objectMode: true,
                transform: function (object, encoding, callback) {
                    consolidate[name](path, object, function (error, html) {
                        callback(error, html);
                    });
                }
            }).on('pipe', function(source) {
                source.on('drain', this.emit.bind(this, 'drain'));
                this.pipe(source);
            });
        };
    })(name);
}
