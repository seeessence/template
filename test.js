var template = require('./index.js');
var unit = require('unit');

unit(/<title>locals<\/title>/, null, {title: 'locals'}).pipe(
	template.jade('test/locals.jade')
);

unit.not(/<title>not locals<\/title>/, null, {title: 'locals'}).pipe(
	template.jade('test/locals.jade')
);
